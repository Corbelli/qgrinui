#!/usr/bin/python
# -*- coding: utf-8 -*-

import functools
import gzip
import optparse
import os, os.path
import re
import sys

if sys.platform == 'win32':
    import ctypes
    import win32gui
    import win32con
else:
    import tempfile
    import shutil

try:
    import PyQt4
    import PyQt4.QtCore as QtCore
    from PyQt4.QtGui import QCursor, QStandardItem, QStandardItemModel, QAction, QApplication, QFileSystemModel, QMainWindow, QWidget
except ImportError:
    try:
        import PyQt5
        import PyQt5.QtCore as QtCore
        import PyQt5.QtGui
        import PyQt5.QtWidgets
        from PyQt5.QtGui import QCursor, QStandardItem, QStandardItemModel
        from PyQt5.QtWidgets import QAction, QApplication, QFileSystemModel, QMainWindow, QWidget
    except ImportError as e:
        print ("PYQT4 and PYQT5 unavailable ({})".format(e))
        sys.exit(1)

import grin

import Qgrin_ui

class CSciteControllerException(Exception):
    pass

class CSciteController_Base:
    DEFAULT_TIMEOUT_MS = 100
    EXE_NAMES = ["SciTE", "scite"]

    def __init__(self, scite_paths, process_timeout):
        self._sciteArgs = []
        self._scitePaths = scite_paths
        self._processTimeout = process_timeout
        self._sciteProcess = None

    def _on_scite_process_exit(self, exit_code, exit_status):
        self._sciteProcess = None

    def __del__(self):
        try: self.quitScite(True)
        except: pass

    def sendCommand(self, command):
        """ Send a command to the scite instance """
        self.startScite()

    def quitScite(self, wait = True):
        """ Quit running scite instance """
        if self._sciteProcess is None:
            return
        if self._sciteProcess.state() != QtCore.QProcess.Running:
            return

        self.sendCommand("quit:")
        if wait:
            if not self._sciteProcess.waitForFinished(self._processTimeout):
                self._sciteProcess.close()

        self._sciteProcess = None
        self._scitePipe = None

    def on_file_position_requested(self, file_name, line):
        """ Open requested file and have scite go to line """
        self.sendCommand("open:{0}\n".format(file_name))
        self.sendCommand("goto:{0}\n".format(line))
        #Toggle Bookmark
        self.sendCommand("menucommand:222\n")
        #Only on GTK the doc says, but no harm on other platforms
        self.sendCommand("focus:\n")

if sys.platform == "win32":
    class CSciteController_Win32(CSciteController_Base, QWidget):
        """ Best use it as a singleton.
            Need to receive window messages, no better idea here
        """
        SCITE_DIRECTOR_PIPE_NAME = "director.hwnd"

        class COPYDATASTRUCT(ctypes.Structure):
            " Only used on win32 "
            _fields_ = [
                ("dwData", ctypes.c_ulong),
                ("cbData", ctypes.c_ulong),
                ("lpData", ctypes.c_void_p)
            ]

        def __init__(self,
                scite_paths = CSciteController_Base.EXE_NAMES,
                process_timeout = CSciteController_Base.DEFAULT_TIMEOUT_MS
            ):
            CSciteController_Base.__init__(self, scite_paths, process_timeout)
            QWidget.__init__(self, None)

            #Don't need anything to show
            self.resize(0, 0)
            #Need to show() to get a window id
            self.show()
            self._parentHwnd = int(self.effectiveWinId())
            #Create a "message-only" window
            #http://msdn.microsoft.com/en-us/library/ms632599%28v=vs.85%29.aspx#message_only
            win32gui.SetParent(self._parentHwnd, win32con.HWND_MESSAGE)
            #Using the process environment does not seem to work
            self._sciteArgs.append("-director.hwnd={0}".format(self._parentHwnd))
            #Will come in later, notified by SciTE. Director window, message
            #only, no user interaction
            self._sciteHwnd = None
            #Main SciTE window that handles user interaction, derived from director
            self._mainSciteHwnd = None

            self._messageHandlers = { "identity" : self.on_identity }

            self.startScite()

        def _on_scite_process_exit(self, exit_code, exit_status):
            CSciteController_Base._on_scite_process_exit(self, exit_code, exit_status)
            self._sciteHwnd = None
            self._mainSciteHwnd = None

        def sendCommand(self, command):
            """ Send a command to the scite instance """
            CSciteController_Base.sendCommand(self, command)

            #If SciTE was killed wait for the new instance to be spawned and
            #"registered"
            #Yes, well, hope that I need no timeout
            while self._sciteHwnd is None:
                QApplication.instance().processEvents(
                    QtCore.QEventLoop.ExcludeUserInputEvents,
                    QtCore.QEventLoop.ExcludeSocketNotifiers
                )

            command = str(command)
            copydatastruct = self.COPYDATASTRUCT(
                dwData = 0,
                cbData = len(command),
                lpData = ctypes.cast(ctypes.c_char_p(command), ctypes.c_void_p)
            )

            win32gui.SendMessage(
                self._sciteHwnd, #Target SciTE window
                win32con.WM_COPYDATA, #Message
                0, #WPARAM
                ctypes.string_at(
                    ctypes.pointer(copydatastruct),
                    ctypes.sizeof(copydatastruct)
                ) #LPARAM
            )

        def startScite(self):
            #Already running
            if self._sciteProcess is not None and self._sciteProcess.state() == QtCore.QProcess.Running:
                return

            self._sciteProcess = QtCore.QProcess()
            for scite_path in self._scitePaths:
                self._sciteProcess.start(scite_path, self._sciteArgs, QtCore.QIODevice.ReadOnly)
                if self._sciteProcess.waitForStarted(self._processTimeout/2):
                    #Started OK
                    break

                if self._sciteProcess.error() != QtCore.QProcess.Timedout:
                    #Likely scite not found, try another path
                    continue

                QtCore.QThread.msleep(self._processTimeout/2)
                break

            if self._sciteProcess.state() != QtCore.QProcess.Running:
                raise CSciteControllerException(
                    "Cannot start scite from {0}: errorcode {1}".format(
                        self._scitePaths, self._sciteProcess.error()
                    )
                )
            self._sciteProcess.finished.connect(self._on_scite_process_exit)

        # Message handlers ---------------------------------------------------------
        def on_identity(self, param):
            "Handle the identify: Scite command"
            #Find the main window
            def callback(window, user_obj):
                class_name = win32gui.GetClassName(window)
                if class_name == "SciTEWindow":
                    user_obj.value = window
                return True

            #Director extension window
            self._sciteHwnd = int(param)

            #Find the main window that handles user interaction
            thread_id = ctypes.windll.user32.GetWindowThreadProcessId(
                self._sciteHwnd, None
            )
            main_window_id = ctypes.c_ulong(0)
            win32gui.EnumThreadWindows(thread_id, callback, main_window_id)
            if main_window_id.value != 0:
                self._mainSciteHwnd = main_window_id.value
            else:
                self._mainSciteHwnd = None

        # --------------------------------------------------------------------------

        def winEvent(self, message):
            """ Filter the WM_COPYDATA message coming from SciTE
            @type message: QtCore.MSG
            """
            if message.message != win32con.WM_COPYDATA:
                #Process with QT
                return (False, 0)
            #Cast to a COPYDATASTRUCT
            copydatastruct_p = ctypes.cast(message.lParam, ctypes.POINTER(self.COPYDATASTRUCT))
            copydatastruct = copydatastruct_p.contents
            if copydatastruct.dwData != 0:
                #Not coming from SciTE
                return (False, 0)

            #Obtain the actual string message
            actual_message = ctypes.string_at(copydatastruct.lpData, copydatastruct.cbData)
            parts = actual_message.split(":")
            if len(parts) < 2:
                #Not coming from SciTE
                return (False, 0)
            # Sample messages
            # identity:8585798
            # opened:C:\\Devel\\Python-2.6.8\\Include\\methodobject.h

            try:
                self._messageHandlers[parts[0]](":".join(parts[1:]))
            except KeyError:
                #no handlers
                pass

            return (True, 0)

        def on_file_position_requested(self, file_name, line):
            """ Open requested file and have scite go to line """
            #Need to transmit escaped backslashes (doubled backslashes)
            file_name = re.escape(os.path.normpath(str(file_name)))
            CSciteController_Base.on_file_position_requested(self, file_name, line)
            win32gui.SetForegroundWindow(self._mainSciteHwnd)

    CSciteController = CSciteController_Win32

else: #Not WIN32 platform
    class CSciteController_Unix(CSciteController_Base, QtCore.QObject):
        """ Best use it as a singleton """

        SCITE_PIPE_ENV_VAR = "ipc.scite.name"
        SCITE_DIRECTOR_ENV_VAR = "ipc.director.name"

        SCITE_PIPE_NAME = "scite.pipe"
        SCITE_DIRECTOR_PIPE_NAME = "director.pipe"

        def __init__(self,
                scite_paths = CSciteController_Base.EXE_NAMES,
                process_timeout = CSciteController_Base.DEFAULT_TIMEOUT_MS
            ):
            CSciteController_Base.__init__(self, scite_paths, process_timeout)
            QtCore.QObject.__init__(self, None)

            self._scitePipe = None

            self.startScite()

        def _on_scite_process_exit(self, exit_code, exit_status):
            """ Remove temp tree """
            CSciteController_Base._on_scite_process_exit(self, exit_code, exit_status)
            self._scitePipe = None
            shutil.rmtree(self._tempDir, True)

        def __del__(self):
            CSciteController_Base.__del__(self)
            shutil.rmtree(self._tempDir, True)

        def sendCommand(self, command):
            """ Send a command to the scite instance """
            CSciteController_Base.sendCommand(self, command)
            command = str(command)
            self._scitePipe.write(command)
            if not command.endswith("\n"):
                self._scitePipe.write("\n")

        def startScite(self):
            #Already running
            if self._sciteProcess is not None and self._sciteProcess.state() == QtCore.QProcess.Running:
                return

            #Need to have a temp path. Can't use mkstemp to create a fifo.
            self._tempDir = tempfile.mkdtemp()
            self._scitePipePath = os.path.join(self._tempDir, self.SCITE_PIPE_NAME)

            env = QtCore.QProcessEnvironment.systemEnvironment()
            #Send commands to Scite
            env.insert(self.SCITE_PIPE_ENV_VAR, self._scitePipePath)
            #Receive notifications, QFile.readyRead does not work
            #env.insert(directorPipe, "/tmp/director.pipe")

            self._sciteProcess = QtCore.QProcess()
            self._sciteProcess.setProcessEnvironment(env)
            for scite_path in self._scitePaths:
                self._sciteProcess.start(scite_path, [], QtCore.QIODevice.ReadOnly)
                if self._sciteProcess.waitForStarted(self._processTimeout/2):
                    #Started OK
                    break

                if self._sciteProcess.error() != QtCore.QProcess.Timedout:
                    #Likely scite not found, try another path
                    continue

                QtCore.QThread.msleep(self._processTimeout/2)
                break

            if self._sciteProcess.state() != QtCore.QProcess.Running:
                raise CSciteControllerException(
                    "Cannot start scite from {0}: errorcode {1}".format(
                        self._scitePaths, self._sciteProcess.error()
                    )
                )
            self._sciteProcess.finished.connect(self._on_scite_process_exit)
            #Create the pipe to command Scite
            #Octal 0600, but don't have to tamper with 0o in python3
            os.mkfifo(self._scitePipePath, 0x180)
            self._scitePipe = open(self._scitePipePath, "w", 0)

            #    director_pipe = QtCore.QFile("/tmp/director.pipe")
            #    director_pipe.open(QtCore.QIODevice.ReadOnly|QtCore.QIODevice.Unbuffered)
            #    director_pipe.readyRead.connect(lambda: sys.stdout.write(director_pipe.readAll()))
            #    director_pipe.readyRead.connect(on_ready_read)

    CSciteController = CSciteController_Unix

class QgrinMainWindow(QMainWindow):
    FULL_PATH_ROLE = QtCore.Qt.UserRole + 1
    " Store full file path in the model "
    LINE_ROLE = FULL_PATH_ROLE + 1
    " Store match line in the model "
    TEXT_ROLE = LINE_ROLE + 1
    " Store match text in the role "

    file_position_requested = QtCore.pyqtSignal(str, int)

    class QgrinFilesystemModel(QFileSystemModel):
        def columnCount(self, parent):
            #Not interested in size/datetime, just dir names
            return 1

    def __init__(self, parent = None, window_flags = QtCore.Qt.Window):
        QMainWindow.__init__(self, parent, window_flags)
        self.ui = Qgrin_ui.Ui_MainWindow()
        self.ui.setupUi(self)

        self._selected_path = QtCore.QDir.currentPath()

        self._grin_openers = dict(
            text=functools.partial(open, errors='surrogateescape'),
            gzip=gzip.open
        )
        #Line numbers
        #Don't skip hidden files
        #Case insensitive
        self._grin_args = [u"-n", u"-s", u"-i"]

        self._fs_model = self.QgrinFilesystemModel()
        self._fs_model.setRootPath(self._selected_path)
        self._fs_model.setFilter(QtCore.QDir.AllDirs|QtCore.QDir.Drives|QtCore.QDir.NoDotAndDotDot|QtCore.QDir.CaseSensitive)

        self._results_model = QStandardItemModel()

        self.ui.FilesystemTreeView.setModel(self._fs_model)
        self.ui.FilesystemTreeView.activated.connect(self._on_FilesystemTreeView_activated)
        self.ui.FilesystemTreeView.clicked.connect(self._on_FilesystemTreeView_activated)
        self.ui.FilesystemTreeView.setCurrentIndex(self._fs_model.index(self._selected_path))
        self.ui.FilesystemTreeView.setFocus(QtCore.Qt.MouseFocusReason)

        self.ui.ResultsTreeView.setModel(self._results_model)
        self.ui.ResultsTreeView.activated.connect(self._on_ResultsTreeView_activated)

        self.ui.FilesystemLabel.setText(self._selected_path)

        action = QAction("Focus Filesystem view", self.ui.FilesystemTreeView)
        action.setShortcut(QtCore.Qt.ALT + QtCore.Qt.Key_1)
        action.triggered.connect(lambda: self.ui.FilesystemTreeView.setFocus(QtCore.Qt.MouseFocusReason))
        self.ui.menuFocus.addAction(action)

        action = QAction("Focus Regex line", self.ui.RegexLineEdit)
        action.setShortcut(QtCore.Qt.ALT + QtCore.Qt.Key_2)
        action.triggered.connect(lambda: self.ui.RegexLineEdit.setFocus(QtCore.Qt.MouseFocusReason))
        self.ui.menuFocus.addAction(action)

        action = QAction("Focus results view", self.ui.ResultsTreeView)
        action.setShortcut(QtCore.Qt.ALT + QtCore.Qt.Key_3)
        action.triggered.connect(lambda: self.ui.ResultsTreeView.setFocus(QtCore.Qt.MouseFocusReason))
        self.ui.menuFocus.addAction(action)

        self.ui.actionE_xit.triggered.connect(lambda: QApplication.instance().exit())

        self.ui.SearchButton.clicked.connect(self._on_SearchButton_clicked)
        self.ui.RegexLineEdit.returnPressed.connect(functools.partial(self._on_SearchButton_clicked, True))
        self.ui.ExitButton.clicked.connect(lambda: QApplication.instance().exit())

    def _on_SearchButton_clicked(self, checked):
        QApplication.setOverrideCursor(QCursor(QtCore.Qt.WaitCursor))
        self.setEnabled(False)
        try:
            self._results_model.clear()

            regex = str(self.ui.RegexLineEdit.text()).strip()
            if len(regex) == 0:
                return

            grin_args = self._grin_args + [regex]
            parser = grin.get_grin_arg_parser()
            try:
                args = parser.parse_args(grin_args)
            except SystemExit as e:
                #If somehow arguments are wrong the parser tries to
                #exit the interpreter
                return
            finally:
                args.use_color = False

            g = grin.GrepText(re.compile(regex), args)
            parent = None
            for filename, kind in grin.get_filenames(args):
                if kind not in self._grin_openers:
                    continue
                report = g.grep_a_file(filename, self._grin_openers[kind])
                # Returns a string object (non unicode in python2)
                for line in report.split(u"\n"):
                    line = line.strip()
                    if len(line) == 0:
                        continue
                    if ":" not in line:
                        continue

                    parts = list(filter(lambda item: len(item) > 0, line.split(":")))
                    isFileName = (len(parts) == 1)
                    if isFileName:
                        fileName = os.path.normpath(os.path.join(self._selected_path, parts[0]))
                        model_item = QStandardItem(os.path.basename(fileName))
                        model_item.setData(fileName, self.FULL_PATH_ROLE)
                        parent = model_item
                        self._results_model.invisibleRootItem().appendRow(model_item)
                    else:
                        lineNumber = int(parts[0].strip())
                        text = line[len(parts[0]):].strip()
                        model_item = QStandardItem("{0} : {1}".format(lineNumber, text))
                        model_item.setData(lineNumber, self.LINE_ROLE)
                        model_item.setData(text, self.TEXT_ROLE)
                        parent.appendRow(model_item)
        finally:
            QApplication.restoreOverrideCursor()
            self.setEnabled(True)
            self.ui.ResultsTreeView.expandAll()
            self.ui.ResultsTreeView.setFocus(QtCore.Qt.MouseFocusReason)

    def _on_FilesystemTreeView_activated(self, model_index):
        self._selected_path = self._fs_model.filePath(model_index)
        self.ui.FilesystemLabel.setText(self._selected_path)
        target_dir = self._selected_path if os.path.isdir(self._selected_path) else os.path.dirname(self._selected_path)
        os.chdir(target_dir)

    def _on_ResultsTreeView_activated(self, model_index):
        file_node_item = model_index.parent()
        #Skip if user clicked on a file
        if file_node_item == QtCore.QModelIndex():
            return
        data_node_item = self._results_model.itemFromIndex(model_index)

        filename = str(file_node_item.data(self.FULL_PATH_ROLE))
        line = int(data_node_item.data(self.LINE_ROLE))
        text = str(data_node_item.data(self.TEXT_ROLE))
        self.file_position_requested.emit(filename, line)

def main():
    parser = optparse.OptionParser()
    parser.add_option(
        "-p", "--scite_paths",
        dest="scite_paths",
        help="Use specific scite PATHs (comma separated)",
        metavar="PATH",
        default="SciTE,scite"
    )
    parser.add_option(
        "-t", "--scite_timeout",
        dest="scite_timeout",
        help="Wait for TIMEOUT ms when starting scite",
        metavar="TIMEOUT",
        default=CSciteController_Base.DEFAULT_TIMEOUT_MS
    )
    (options, args) = parser.parse_args()

    app = QApplication(args)
    MainWindow = QgrinMainWindow()
    SciteController = CSciteController(
        options.scite_paths.split(","),
        int(options.scite_timeout)
    )
    MainWindow.show()

    MainWindow.file_position_requested.connect(SciteController.on_file_position_requested)
    app.aboutToQuit.connect(SciteController.quitScite)

    return app.exec_()

if __name__ == '__main__':
    os._exit(main())
