#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys

from setuptools import setup

from PyQt4 import uic

VERSION = "0.1"

EGG = len(sys.argv) > 1 and sys.argv[-1] == 'bdist_egg'
DIST = len(sys.argv) > 1 and sys.argv[-1] == 'bdist'
WININST = len(sys.argv) > 1 and sys.argv[-1] == 'bdist_wininst'

WINDOWS = MAC = UNIX = False
if sys.platform.startswith('win'):
    WINDOWS = True
elif sys.platform.startswith('darwin'):
    MAC = True
else:
    UNIX = True

install_requires = [
    "grin >= 1.2.1",
]

data_files = [
    ('share/Qgrinui', ['Qgrin.ui']),
]

egg_data_files = [
    ('Qgrinui/share/Qgrinui', ['Qgrin.ui']),
]

scripts = []

entry_points = {
    'gui_scripts': [
        'Qgrin = Qgrin:main',
    ]
}

if WINDOWS:
    if EGG:
        data_files = egg_data_files
    elif WININST:
        data_files = data_files

elif MAC:
    raise Exception("Mac not implemented")

elif UNIX:
    if EGG:
        data_files = egg_data_files
        entry_points['setuptools.installation'] = ['eggsecutable = Qgrin:main']

#-------------------------------------------------------------------------------
packages = []

modules = [
    'Qgrin',
    'Qgrin_ui',
]

#Build the python GUI module from .ui
pyfile = open("Qgrin_ui.py", 'wt')
uic.compileUi("Qgrin.ui", pyfile, True)
pyfile.close()

setup (
    name = 'qgrinui',
    version = VERSION,
    url = 'https://gitorious.org/qgrinui/qgrinui',
    author = "Giuseppe Corbelli",
    author_email = "cowo78@gmail.com",
    description = 'pyQt grin user interface',
    platforms = ['linux', 'win32'],
    packages = packages,
    package_dir = {'Qgrin': ''},
    py_modules = modules,
    scripts = scripts,
    cmdclass = {},
    data_files = data_files,
    include_package_data = False,
    zip_safe = True,
    install_requires = install_requires,
    entry_points = entry_points,
    keywords = """
Metadata-Version: 1.1
Name: Qgrinui
Version: {0}
Provides: Qgrinui
{1}
    """.format(
        VERSION,
        "\n".join(map(lambda package: "Requires: {0}".format(package), install_requires))
    )
)
